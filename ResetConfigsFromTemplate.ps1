﻿Write-Host "###Reset Configs From Template###"

[string[]]$Paths = @('.\')
[string[]]$FileIncludes = @('*.template.config')
[string[]]$FileExcludes = @('*bin*', '*obj*', '*packages*', '*Scripts*', '*Content*', '*.nuget*', '*fonts*')

$files = Get-ChildItem $Paths -File -Recurse -Exclude $FileExcludes -Include $FileIncludes | 
    %{$_.FullName} | 
    Sort-Object -Property Length -Descending | 
%{ 
    $Item = Get-Item $_
    $allowed = $true
    foreach ($exclude in $Excludes) { 
        if ((Split-Path $Item.FullName -Parent) -ilike $exclude) { 
            $allowed = $false
            break
        }
    }
    if ($allowed) {
        $Item
    }
}

Write-Host "Refactoring all solution files"
foreach ($f in $files) {
    $newName = ""
    if ($f.Name.ToLower() -eq "app.template.config") {
        $newName = "app.config"
    } elseif ($f.Name.ToLower() -eq "web.template.config") {
        $newName = "Web.config"
    }
    $newPath = "$($f.Directory)\$($newName)"
    Write-Host  $newPath "->" $f.Name
    Copy-Item $f.FullName $newPath
}

