﻿using BrownSignApp.Services;

namespace BrownSignApp.CreatePreviewImages {
    class Program {
        static void Main(string[] args) {
            LoggerService loggerService = new LoggerService();
            loggerService.Log("CreatePreviewImages starting...");
            DocumentService documentService = new DocumentService();
            documentService.CreatePreviewImages();
            loggerService.Log("CreatePreviewImages finished...");
        }
    }
}
