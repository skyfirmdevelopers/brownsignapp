﻿using BrownSignApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.SignDocuments {
    class Program {
        static void Main(string[] args) {
            LoggerService loggerService = new LoggerService();
            loggerService.Log("SignDocuments starting...");
            DocumentService documentService = new DocumentService();
            documentService.SignDocuments();
            loggerService.Log("SignDocuments finished...");
        }
    }
}
