﻿using BrownSignApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.PrepareZipFiles {
    class Program {
        static void Main(string[] args) {
            LoggerService loggerService = new LoggerService();
            loggerService.Log("PrepareZipFiles starting...");
            DocumentService documentService = new DocumentService();
            documentService.PrepareSignedDocuments();
            loggerService.Log("PrepareZipFiles finished...");
        }
    }
}
