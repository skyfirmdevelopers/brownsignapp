﻿function OpenAddManageUserModal() {
    OpenModal("/Admin/User/Create", "Create User", AddManageUserModalLoaded, AddManageUserModalDismissed)
}

function OpenEditManageUserModal(userId) {
    OpenModal("/Admin/User/Edit?userId=" + userId, "Edit User", AddManageUserModalLoaded, AddManageUserModalDismissed)
}

function OpenResetPasswordModal(userId) {
    OpenModal("/Admin/User/ResetPassword?userId=" + userId, "Reset Password", ResetPasswordModalLoaded, ResetPasswordModalDismissed)
}

function AddManageUserModalLoaded() {
    
}

function AddManageUserModalDismissed() {
    
}

function ResetPasswordModalLoaded() {

}

function ResetPasswordModalDismissed() {

}

function AddManageUserOnSuccess() {

}

function AddManageUserOnFailure() {

}

function EditManageUserOnSuccess() {

}

function EditManageUserOnFailure() {

}