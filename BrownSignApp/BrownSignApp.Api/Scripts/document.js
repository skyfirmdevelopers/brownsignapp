﻿$(function () {
    $("#document_container ul li:first a").addClass("class", "active");
    $("#document_container ul li:first a").click();

    setInterval(function () {
        GetDocumentSignProgress()
    }, 5000);
})

function OpenAddDocumentModal() {
    OpenModal("/Document/Create", "Upload Document", AddDocumentModalLoaded, AddDocumentModalDismissed)
}

function OpenEditDocumentModal(documentId) {
    OpenModal("/Document/Edit?documentId=" + documentId, "Edit Document", AddDocumentModalLoaded, AddDocumentModalDismissed)
}

function OpenDocumentPreview(documentId) {
    OpenDisplayModal("/Document/PreviewDocument?documentId=" + documentId, "Preview Document", AddDocumentModalLoaded, AddDocumentModalDismissed)
}

function AddDocumentModalLoaded() {

}

function AddDocumentModalDismissed() {

}

function AddDocumentOnSuccess() {

}

function AddDocumentOnFailure() {

}

function EditDocumentOnSuccess() {

}

function EditDocumentOnFailure() {

}

function SelectAllDocuments() {
    $("#documents_to_sign td input").each(function () {
        var isChecked = $(this).prop("checked");
        if (isChecked == false) {
            $(this).prop("checked", true);
        } else if (isChecked == true) {
            $(this).prop("checked", false);
        }
    });
}

function SignSelectedDocuments() {
    var documentIds = [];
    $("#documents_to_sign td input").each(function () {
        if ($(this).prop("checked") == true) {
            var documentId = $(this).val();
            documentIds.push(documentId);
        }
    });
    $.ajax({
        url: "/Document/SignSelectedDocuments",
        type: "POST",
        data: { DocumentIds: documentIds },
        success: function (data) {
            $("#tab_container")
                .empty()
                .html(data);
        }
    });
}


function GetDocumentSignProgress() {
    $.ajax({
        url: "/Document/DocumentSignProgress",
        type: "GET",
        success: function (data) {
            var lastProgress = parseInt($("#LastProgress").val());
            if (data > 0) {
                $("#progress_bar .progress-bar").attr("style", "width:" + data + "%");
                $("#progress_bar .progress-bar").attr("aria-valuenow", data);
                $("#progress_bar .progress-bar").html(data + "% Complete");
                
                if (lastProgress == 100 && data == 100) {
                    $("#progress_text").text("Signing Complete!");
                    $("#progress_bar").hide();
                    window.location.reload(true);
                } else if (lastProgress == 0 && data == 100) {
                    // do nothing
                } else {
                    $("#progress_bar").show();
                    $("#progress_text").text("Signing Documents...");
                    $("#LastProgress").val(data);
                }
                
            }
        }
    });
}