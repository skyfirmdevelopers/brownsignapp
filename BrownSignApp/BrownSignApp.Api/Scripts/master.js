﻿
function OpenDisplayModal(url, title, modalLoadedCallback, modalDismissedCallback) {
    $("#DisplayModal").modal("show");
    $("#DisplayModal").on("hide.bs.modal", function (e) {
        if (modalDismissedCallback != null) {
            modalDismissedCallback();
        }
    });
    $.ajax({
        url: url,
        type: "GET",
        success: function (data) {
            $(".modal-title", "#DisplayModal").html(title);
            $(".modal-body", "#DisplayModal").html(data);
            $("form", "#DisplayModal").removeData("validator");
            $("form", "#DisplayModal").removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse("form", "#DisplayModal");
            $("#modal-save", "#DisplayModal").unbind("click").click(function () {
                $("form", "#DisplayModal").validate();
                if ($("form", "#DisplayModal").valid()) {
                    $("form", "#DisplayModal").submit();
                    $("#DisplayModal").modal("hide");
                    $(".modal-body", "#DisplayModal").empty();
                }
            });
            if (modalLoadedCallback != null) {
                modalLoadedCallback();
            }
        }
    });
}

function OpenModal(url, title, modalLoadedCallback, modalDismissedCallback) {
    $("#DefaultModal").modal("show");
    $("#DefaultModal").on("hide.bs.modal", function (e) {
        if (modalDismissedCallback != null) {
            modalDismissedCallback();
        }
    });
    $.ajax({
        url: url,
        type: "GET",
        success: function (data) {
            $(".modal-title", "#DefaultModal").html(title);
            $(".modal-body", "#DefaultModal").html(data);
            $("form", "#DefaultModal").removeData("validator");
            $("form", "#DefaultModal").removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse("form", "#DefaultModal");
            $("#modal-save", "#DefaultModal").unbind("click").click(function () {
                $("form", "#DefaultModal").validate();
                if ($("form", "#DefaultModal").valid()) {
                    $("form", "#DefaultModal").submit();
                    $("#DefaultModal").modal("hide");
                    $(".modal-body", "#DefaultModal").empty();
                }
            });
            if (modalLoadedCallback != null) {
                modalLoadedCallback();
            }
        }
    });
}

function OpenTab(url, container, parentcontainer) {
    $("#" + container).remove();
    $.ajax({
        url: url,
        type: "GET",
        success: function (data) {
            var div = "<div id='" + container + "'>" + data + "</div>";
            $("#" + parentcontainer).append(div);
        }
    });
}