﻿function OpenAddLocationModal() {
    OpenModal("/Admin/Location/Create", "Create Location", AddLocationModalLoaded, AddLocationModalDismissed);
}

function OpenEditLocationModal(locationId) {
    OpenModal("/Admin/Location/Edit?locationId=" + locationId, "Location User", EditLocationModalLoaded, EditLocationModalDismissed);
}

function AddLocationModalLoaded() { }

function AddLocationModalDismissed() { }

function AddLocationOnSuccess() { }

function AddLocationOnFailure() { }

function EditLocationModalLoaded() { }

function EditLocationModalDismissed() { }

function EditLocationOnSuccess() { }

function EditLocationOnFailure() { }