﻿using BrownSignApp.Api.Helpers;
using BrownSignApp.Api.Models;
using BrownSignApp.Data;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrownSignApp.Api.Views.Shared {
    public abstract class AspViewPage<TModel> : WebViewPage<TModel> {

        public CurrentUser CurrentUser {
            get {
                return new Utility().CurrentUser(User);
            }
        }
        
    }
}