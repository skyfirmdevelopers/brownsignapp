﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrownSignApp.Api.Views.Models {
    public class DocumentUpload {

        [Required]
        public HttpPostedFileBase[] docs { get; set; }

    }
}