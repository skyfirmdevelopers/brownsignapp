﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrownSignApp.Api.Helpers {
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorize : AuthorizeAttribute {

        private string[] UserProfilesRequired { get; set; }

        public CustomAuthorize(params string[] userProfilesRequired) {
            UserProfilesRequired = userProfilesRequired;
        }

        public override void OnAuthorization(AuthorizationContext filterContext) {
            var CurrentUser = new Utility().CurrentUser(filterContext.HttpContext.User);
            var userRoles = CurrentUser.Roles; // ideally this is pointed at a permission claim type, revisit later
            if (filterContext.HttpContext.Request.IsAuthenticated == false) {
                string retUrl = filterContext.HttpContext.Request.RawUrl;
                filterContext.Result = new RedirectToRouteResult(
                    "Default",
                    new System.Web.Routing.RouteValueDictionary {
                        { "controller", "User" },
                        { "action", "Login"},
                        { "returnUrl", retUrl}
                    }
                );
                return;
            }
            if (!UserProfilesRequired.Any(x => userRoles.Any(t => t == x))) {
                string retUrl = filterContext.HttpContext.Request.RawUrl;
                filterContext.Result = new RedirectToRouteResult(
                    "Default",
                    new System.Web.Routing.RouteValueDictionary {
                        { "controller", "Error" },
                        { "action", "AccessDenied"},
                        { "returnUrl", retUrl}
                    }
                );
            }
        }

    }
}
