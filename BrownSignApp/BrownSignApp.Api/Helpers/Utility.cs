﻿using BrownSignApp.Api.Models;
using BrownSignApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace BrownSignApp.Api.Helpers {
    public class Utility {

        public CurrentUser CurrentUser(System.Security.Principal.IPrincipal User) {
            CurrentUser user = new CurrentUser();
            ClaimsPrincipal principal = User as ClaimsPrincipal;

            if (principal != null) {
                foreach (Claim claim in principal.Claims) {
                    switch (claim.Type) {
                        case "UserId":
                            int userId = Convert.ToInt32(claim.Value);
                            user.UserId = userId;
                            break;
                        case "Username":
                            user.Username = claim.Value;
                            break;
                        case "FirstName":
                            user.FirstName = claim.Value;
                            break;
                        case "LastName":
                            user.LastName = claim.Value;
                            break;
                        case "Email":
                            user.Email = claim.Value;
                            break;
                        case "LocationId":
                            int locationId = Convert.ToInt32(claim.Value);
                            user.LocationId = locationId;
                            break;
                        case "Roles":
                            user.Roles.Add(claim.Value);
                            break;
                    }
                }
            }
            return user;

        }

    }
}