﻿using BrownSignApp.Api.Helpers;
using BrownSignApp.Controllers;
using BrownSignApp.Data;
using BrownSignApp.Services;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace BrownSignApp.Api.Controllers {
    [CustomAuthorize("DocumentUploader", "DocumentSigner", "DocumentDownloader", "Admin")]
    public class DocumentController : BaseController {
        DocumentService documentService = new DocumentService();

        [CustomAuthorize("DocumentUploader", "DocumentSigner", "DocumentDownloader", "Admin")]
        public ActionResult Index() {
            return View();
        }

        [CustomAuthorize("DocumentSigner", "Admin")]
        public ActionResult DocumentsToSign() {
            var result = documentService.GetRequiredSignatureDocuments();
            return PartialView(result);
        }

        [CustomAuthorize("DocumentUploader", "Admin")]
        public ActionResult Unsigned() {
            var result = documentService.GetUnsignedDocuments(CurrentUser.LocationId);
            return PartialView(result);
        }
        
        [CustomAuthorize("DocumentDownloader", "Admin")]
        public ActionResult Signed() {
            var result = documentService.GetSignedZips(CurrentUser.LocationId);
            return PartialView(result);
        }

        [CustomAuthorize("DocumentUploader", "Admin")]
        public ActionResult Create() {
            return PartialView();
        }
        
        [CustomAuthorize("DocumentUploader", "Admin")]
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase[] docs) {
            var uploadDocs = new List<UploadFile>();
            foreach (HttpPostedFileBase doc in docs)
            {
                uploadDocs.Add(new UploadFile() { 
                    FileName = doc.FileName,
                    InputStream = doc.InputStream,
                });
            }
            var result = documentService.SaveDocuments(uploadDocs.ToArray(), CurrentUser.UserId, CurrentUser.LocationId);
            return RedirectToAction("Index", "Document");
        }

        [HttpPost]
        [CustomAuthorize("DocumentSigner", "Admin")]
        public ActionResult SignSelectedDocuments(int[] DocumentIds) {
            var result = documentService.UpdateDocumentsForSigning(CurrentUser.UserId, DocumentIds);
            return PartialView("DocumentsToSign", result);
        }

        [HttpGet]
        [CustomAuthorize("DocumentSigner", "Admin")]
        public ActionResult DocumentSignProgress() {
            var result = documentService.DocumentSignProgress();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize("DocumentDownloader", "Admin")]
        public FileResult DownloadZip(int zipFileId) {
            string fileName = string.Empty;
            var result = documentService.GetZipForDownload(zipFileId, CurrentUser.UserId, out fileName);
            return File(result, "application/zip", fileName);
        }

        [CustomAuthorize("DocumentPreviewer", "Admin")]
        public ActionResult PreviewDocument(int documentId) {
            List<string> images = documentService.GetImagesForPreview(documentId);
            return PartialView(images);
        }

        [CustomAuthorize("DocumentUploader", "Admin")]
        public ActionResult RemoveDocument(int documentId) {
            documentService.DeleteDocument(documentId, CurrentUser.LocationId);
            return RedirectToAction("Index", "Document");
        }

    }
}