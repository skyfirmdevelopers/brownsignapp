using BrownSignApp.Controllers;
using BrownSignApp.Data;
using BrownSignApp.Services;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Web.Mvc;
using System.Linq;

namespace BrownSignApp.Api.Controllers {
    public class UserController : BaseController {
        UserService userService = new UserService();

        public ActionResult Login() {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User model) {
            var result = userService.Login(model);
            if (result != null) {
                var claimsId = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);

                claimsId.AddClaim(new Claim("UserId", result.UserId.ToString()));
                claimsId.AddClaim(new Claim("Username", result.Username));
                claimsId.AddClaim(new Claim("Email", result.Email));
                claimsId.AddClaim(new Claim("FirstName", result.FirstName));
                claimsId.AddClaim(new Claim("LastName", result.LastName));
                claimsId.AddClaim(new Claim("LocationId", result.LocationId.ToString()));

                foreach (UserRole role in result.UserRoles) {
                    claimsId.AddClaim(new Claim("Roles", role.Role.Name));
                }

                AuthenticationManager.SignIn(claimsId);

                if (result.UserRoles.Any(x=> x.Role.Name.ToLower().Contains("admin"))) {
                    return RedirectToAction("Index", "User", new { area = "Admin" });
                }
                return RedirectToAction("Index", "Document");
            }
            return View();
        }

        public ActionResult Logout() {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "User");
        }

    }
}