﻿using System.Web.Mvc;

namespace BrownSignApp.Api.Controllers {
    public class ErrorController : Controller {

        public ActionResult AccessDenied() {
            return View();
        }
    }
}