using BrownSignApp.Api.Helpers;
using BrownSignApp.Api.Models;
using Microsoft.Owin.Security;
using System;
using System.Data.Entity.Validation;
using System.Web;
using System.Web.Mvc;

namespace BrownSignApp.Controllers {
    public class BaseController : Controller {
        public BaseController() {

        }

        public IAuthenticationManager AuthenticationManager {
            get {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        public CurrentUser CurrentUser {
            get {
                return new Utility().CurrentUser(User);
            }
        }


        public string GetEntityValidationErrors(DbEntityValidationException exception) {
            string errors = string.Empty;
            foreach (var validationErrors in exception.EntityValidationErrors) {
                foreach (var validationError in validationErrors.ValidationErrors) {
                    string message = string.Format("{0}:{1}",
                        validationErrors.Entry.Entity.ToString(),
                        validationError.ErrorMessage);
                    errors += message + Environment.NewLine;
                }
            }
            return errors;
        }

    }
}