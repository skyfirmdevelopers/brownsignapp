﻿using BrownSignApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrownSignApp.Api.Areas.Admin.Models
{
   public class CreateLocationViewModel
   {
      public CreateLocationViewModel()
      {
            Location = new Location();
      }

      public Location Location { get; set; }
   }
}