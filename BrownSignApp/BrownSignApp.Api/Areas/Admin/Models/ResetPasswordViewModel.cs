﻿using System.ComponentModel.DataAnnotations;

namespace BrownSignApp.Api.Areas.Admin.Models {
    public class ResetPasswordViewModel {

        [Required]
        public int UserId { get; set; }

        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}