﻿using BrownSignApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrownSignApp.Api.Areas.Admin.Models {
    public class CreateUserViewModel {

        public CreateUserViewModel() {
            RoleIds = new List<int>();
        }

        public User User { get; set; }

        [Required]
        [Display(Name = "Roles : ")]
        public List<int> RoleIds { get; set; }

        public List<Role> Roles {
            get {
                using (StorageEntities db = new StorageEntities()) {
                    return db.Roles.ToList();
                }
            }
        }

        public List<Location> Locations {
            get {
                using (StorageEntities db = new StorageEntities()) {
                    return db.Locations.ToList();
                }
            }
        }

        public List<SelectListItem> RoleSelectList {
            get {
                var list = Roles
                    .Select(x => new SelectListItem { Text = x.Name, Value = x.RoleId.ToString() })
                    .ToList();
                foreach (int roleId in RoleIds) {
                    foreach (SelectListItem item in list) {
                        if (item.Value == roleId.ToString())
                            item.Selected = true;
                    }
                }
                return list;
            }
        }

        public IEnumerable<SelectListItem> LocationSelectList {
            get {
                return Locations.Select(x => new SelectListItem { Text = x.Name, Value = x.LocationId.ToString() });
            }
        }

    }
}