using BrownSignApp.Api.Areas.Admin.Models;
using BrownSignApp.Api.Helpers;
using BrownSignApp.Controllers;
using BrownSignApp.Services;
using System.Linq;
using System.Web.Mvc;

namespace BrownSignApp.Api.Areas.Admin.Controllers {
    [CustomAuthorize("Admin")]
    public class UserController : BaseController {
        UserService userService = new UserService();

        [Authorize]
        public ActionResult Index() {
            var result = userService.GetAllUsers();
            return View(result);
        }

        //[Authorize]
        public ActionResult Create() {
            return PartialView(new CreateUserViewModel());
        }

        //[Authorize]
        [HttpPost]
        public ActionResult Create(CreateUserViewModel model) {
            var result = userService.Register(model.User, model.RoleIds);
            return PartialView("AllUsers", result);
        }

        [Authorize]
        public ActionResult Delete(int userId) {
            userService.DeleteUser(userId);
            return RedirectToAction("Index", "User");
        }

        [Authorize]
        public ActionResult Edit(int UserId) {
            CreateUserViewModel model = new CreateUserViewModel();
            model.User = userService.GetUserByUserId(UserId);
            var userRoles = userService.GetUserRolesByUserId(UserId);
            model.RoleIds = userRoles
                .Select(x => x.RoleId)
                .ToList();
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(CreateUserViewModel model) {
            var result = userService.Edit(model.User, model.RoleIds);
            return PartialView("AllUsers", result);
        }

        [Authorize]
        public ActionResult ResetPassword(int UserId) {
            return PartialView("ResetPassword", new ResetPasswordViewModel() { UserId = UserId });
        }

        [Authorize]
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model) {
            userService.ResetPassword(model.UserId, model.Password);
            return Json(true);
        }

    }
}