﻿using BrownSignApp.Api.Areas.Admin.Models;
using BrownSignApp.Api.Helpers;
using BrownSignApp.Controllers;
using BrownSignApp.Services;
using System.Web.Mvc;

namespace BrownSignApp.Api.Areas.Admin.Controllers {
    [CustomAuthorize("Admin")]
    public class LocationController : BaseController {
        LocationService locationService = new LocationService();

        [Authorize]
        public ActionResult Index() {
            var result = locationService.GetAllLocations();
            return View(result);
        }

        [Authorize]
        public ActionResult Create() {
            return PartialView(new CreateLocationViewModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(CreateLocationViewModel model) {
            var result = locationService.Create(model.Location);
            return PartialView("AllLocations", result);
        }

        [Authorize]
        public ActionResult Delete(int locationId) {
            locationService.Delete(locationId);
            return RedirectToAction("Index", "Location");
        }

        [Authorize]
        public ActionResult Edit(int LocationId) {
            CreateLocationViewModel model = new CreateLocationViewModel();
            model.Location = locationService.GetLocationByLocationId(LocationId);
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(CreateLocationViewModel model) {
            var result = locationService.Edit(model.Location);
            return PartialView("AllLocations", result);
        }

    }
}