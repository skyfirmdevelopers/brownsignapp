﻿CREATE PROCEDURE GetSignedProgress
	
AS 
BEGIN
	DECLARE @totalDocs int, @signedDocs int

	set @signedDocs = (	select	count(*) 
						from	Document
						where	StatusId = 2)
	set @totalDocs = ( select count(*)
						from Document
						where StatusId = 6 or StatusId = 2)
	if (@signedDocs = 0 and @totalDocs = 1)
	BEGIN 
		select 1.0
		return
	END
	if (@signedDocs = 0 and @totalDocs > 0 or 
		@signedDocs > 0 and @totalDocs > 0)
	BEGIN 
		select convert(decimal(18,2), @signedDocs/(@totalDocs * 1.0) * 100)
		return
	END
	select 0.0
END
