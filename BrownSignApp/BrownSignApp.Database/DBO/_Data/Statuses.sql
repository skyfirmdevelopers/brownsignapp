﻿PRINT 'Importing [dbo].[Status]'
CREATE TABLE #TempImportStatus (
    [StatusId]    INT      NOT NULL   PRIMARY KEY CLUSTERED,
    [Name]  NVARCHAR(50)   NOT NULL,
)
GO
INSERT #TempImportStatus ([StatusId], [Name]) VALUES (1, N'Pending')
GO
INSERT #TempImportStatus ([StatusId], [Name]) VALUES (2, N'Signed')
GO
INSERT #TempImportStatus ([StatusId], [Name]) VALUES (3, N'Downloaded')
GO
INSERT #TempImportStatus ([StatusId], [Name]) VALUES (4, N'Zipped')
GO
INSERT #TempImportStatus ([StatusId], [Name]) VALUES (5, N'Ready')
GO
INSERT #TempImportStatus ([StatusId], [Name]) VALUES (6, N'Staged For Sign')
GO

DELETE FROM [dbo].[Status]
WHERE [StatusId] NOT IN (
    SELECT [StatusId]
    FROM #TempImportStatus
)
GO
SET IDENTITY_INSERT [dbo].[Status] ON
MERGE INTO [dbo].[Status] AS destination
USING (SELECT * FROM #TempImportStatus) AS source
ON (destination.[StatusId] = source.[StatusId])
    WHEN MATCHED THEN
        UPDATE SET
            [Name] = source.[Name]
    WHEN NOT MATCHED THEN
        INSERT ([StatusId], [Name])
        VALUES (source.[StatusId], source.[Name]);
SET IDENTITY_INSERT [dbo].[Status] OFF
DROP TABLE #TempImportStatus
GO