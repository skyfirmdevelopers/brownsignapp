﻿IF NOT EXISTS(SELECT TOP 1 * FROM [User] where Username = 'admin')
BEGIN
DECLARE @username varchar(60), @roleId int
--Password = 
set @username = 'admin'
set @roleId = 1

DECLARE @admin_user_id int
-- Insert admin user record
insert into [User] 
values ('Admin', 'User', 'it@sky-firm.com', 'admin', 'AB3ORODEqbQJXhbKJ1nTk2ZPyCxYNqIoIgoDOJ87DoUWnzX9s2etjhHeEXgdC3+1/A==', 1, 1, NULL, NULL)

set @admin_user_id = (select top 1 UserId from [User] where Username = @username)

insert into UserRole
values (@roleId, @admin_user_id)
END