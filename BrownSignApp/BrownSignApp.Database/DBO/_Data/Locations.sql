﻿PRINT 'Importing [dbo].[Location]'
CREATE TABLE #TempImportLocation (
    [LocationId]   INT            NOT NULL   PRIMARY KEY CLUSTERED,
    [Name]         NVARCHAR(20)   NOT NULL,
	[Address]      NVARCHAR(80)   NOT NULL,
	[IsActive]     BIT            NOT NULL,
)
GO
INSERT #TempImportLocation ([LocationId], [Name], [Address], [IsActive]) 
VALUES (1, N'Default', '', 1)
GO

SET IDENTITY_INSERT [dbo].[Location] ON
MERGE INTO [dbo].[Location] AS destination
USING (SELECT * FROM #TempImportLocation) AS source
ON (destination.[LocationId] = source.[LocationId])
    WHEN MATCHED THEN
        UPDATE SET
            [Name] = source.[Name],
			[Address] = source.[Address],
			[IsActive] = source.[IsActive]
    WHEN NOT MATCHED THEN
        INSERT ([LocationId], [Name], [Address], [IsActive])
        VALUES (source.[LocationId], source.[Name], source.[Address], source.[IsActive]);
SET IDENTITY_INSERT [dbo].[Location] OFF
DROP TABLE #TempImportLocation
GO