﻿PRINT 'Importing [dbo].[Role]'
CREATE TABLE #TempImportRole (
    [RoleId]    INT      NOT NULL   PRIMARY KEY CLUSTERED,
    [Name]  NVARCHAR(20)   NOT NULL,
)
GO
INSERT #TempImportRole ([RoleId], [Name]) VALUES (1, N'Admin')
GO
INSERT #TempImportRole ([RoleId], [Name]) VALUES (2, N'DocumentUploader')
GO
INSERT #TempImportRole ([RoleId], [Name]) VALUES (3, N'DocumentDownloader')
GO
INSERT #TempImportRole ([RoleId], [Name]) VALUES (4, N'DocumentSigner')
GO
INSERT #TempImportRole ([RoleId], [Name]) VALUES (5, N'DocumentPreviewer')
GO
DELETE FROM [dbo].[Role]
WHERE [RoleId] NOT IN (
    SELECT [RoleId]
    FROM #TempImportRole
)
GO
SET IDENTITY_INSERT [dbo].[Role] ON
MERGE INTO [dbo].[Role] AS destination
USING (SELECT * FROM #TempImportRole) AS source
ON (destination.[RoleId] = source.[RoleId])
    WHEN MATCHED THEN
        UPDATE SET
            [Name] = source.[Name]
    WHEN NOT MATCHED THEN
        INSERT ([RoleId], [Name])
        VALUES (source.[RoleId], source.[Name]);
SET IDENTITY_INSERT [dbo].[Role] OFF
DROP TABLE #TempImportRole
GO