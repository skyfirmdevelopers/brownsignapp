﻿CREATE TABLE [dbo].[User]
(
	[UserId]         INT           NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [FirstName]      VARCHAR(50)   NOT NULL, 
    [LastName]       VARCHAR(50)   NOT NULL, 
    [Email]          VARCHAR(50)   NOT NULL, 
    [Username]       NVARCHAR(50)  NOT NULL, 
    [Password]       NVARCHAR(100) NOT NULL, 
	[IsActive]       BIT           NOT NULL,
    [LocationId]     INT           NOT NULL,
	[ModifiedDate]   DATETIME          NULL,
	[ModifiedBy]     INT               NULL,

	CONSTRAINT [FK_User_Location] 
	FOREIGN KEY ([LocationId]) 
	REFERENCES [Location]([LocationId]),

	CONSTRAINT [FK_User_ModifiedBy] 
	FOREIGN KEY ([ModifiedBy]) 
	REFERENCES [User]([UserId]),


)
