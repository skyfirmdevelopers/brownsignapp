﻿CREATE TABLE [dbo].[Document]
(
	[DocumentId]       INT          NOT NULL PRIMARY KEY IDENTITY(1,1),
    [LocationId]       INT          NOT NULL, 
	[Name]             VARCHAR(100) NOT NULL,
	[Extension]        VARCHAR(5)   NOT NULL,
	[IsActive]         BIT          NOT NULL,
	[HasPreview]       BIT          NOT NULL,
    [StatusId]         INT          NOT NULL, 
	[UploadDate]       DATETIME     NOT NULL,
	[UploadBy]         INT          NOT NULL,
	[SignedDate]       DATETIME         NULL,
	[SignedBy]         INT              NULL,
	[ZippedDate]       DATETIME         NULL,
	[ZippedBy]         INT              NULL,
	[ModifiedDate]     DATETIME         NULL,
	[ModifiedBy]       INT              NULL,

    CONSTRAINT [FK_Documents_Location] FOREIGN KEY ([LocationId]) REFERENCES [Location]([LocationId]), 
    CONSTRAINT [FK_Documents_Status] FOREIGN KEY ([StatusId]) REFERENCES [Status]([StatusId]),
	CONSTRAINT [FK_Documents_UploadBy] FOREIGN KEY ([UploadBy]) REFERENCES [User]([UserId]),
	CONSTRAINT [FK_Documents_SignedBy] FOREIGN KEY ([SignedBy]) REFERENCES [User]([UserId]),
	CONSTRAINT [FK_Documents_ModifiedBy] FOREIGN KEY ([ModifiedBy]) REFERENCES [User]([UserId]),
)
