﻿CREATE TABLE [dbo].[ZipFileDocument]
(
	[ZipFileDocumentId] INT      NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ZipFileId]         INT      NOT NULL,
	[DocumentId]        INT      NOT NULL,
	[CreatedDate]       DATETIME NOT NULL,
)
