﻿CREATE TABLE [dbo].[Location]
(
	[LocationId]       INT          NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Name]             NVARCHAR(50) NOT NULL,
	[Address]          NVARCHAR(80) NOT NULL,
	[IsActive]         BIT          NOT NULL,
	[ModifiedDate]     DATETIME         NULL,
	[ModifiedBy]       INT              NULL,

	CONSTRAINT [FK_Location_ModifiedBy]
	FOREIGN KEY ([ModifiedBy])
	REFERENCES [dbo].[User] ([UserId])
)
