﻿CREATE TABLE [dbo].[UserRole]
(
	[UserRoleId] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [RoleId]     INT NOT NULL, 
    [UserId]     INT NOT NULL
	CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleId]) REFERENCES [Role]([RoleId]), 
	CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([UserId]) REFERENCES [User]([UserId]), 
)
