﻿CREATE TABLE [dbo].[ZipFile]
(
	[ZipFileId]        INT          NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[LocationId]       INT          NOT NULL,
	[Name]             VARCHAR(100) NOT NULL,
	[StatusId]         INT          NOT NULL,
	[CreatedDate]      DATETIME     NOT NULL,
	[DownloadDate]     DATETIME         NULL,
	[DownloadBy]       INT              NULL,
)
