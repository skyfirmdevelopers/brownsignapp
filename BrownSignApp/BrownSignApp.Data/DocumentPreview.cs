﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.Data {
    public class DocumentPreview {

        public int DocumentId { get; set; }
        public string DocumentImage { get; set; }

    }
}
