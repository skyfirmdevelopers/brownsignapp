﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.Data
{
    public class UploadFile
    {
        public string FileName { get; set; }
        public Stream InputStream { get; set; }
    }
}
