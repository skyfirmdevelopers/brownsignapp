﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.Data {

    [Table("ZipFileDocument")]
    public class ZipFileDocument {

        [Key]
        public int ZipFileDocumentId { get; set; }

        public int ZipFileId { get; set; }
        public int DocumentId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
