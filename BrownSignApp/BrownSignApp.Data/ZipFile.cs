﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.Data {

    [Table("ZipFile")]
    public class ZipFile {
        [Key]
        public int ZipFileId { get; set; }
        public int LocationId { get; set; }
        public string Name { get; set; }
        public int StatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? DownloadDate { get; set; }
        public int? DownloadBy { get; set; }

        public virtual Status Status { get; set; }
        [ForeignKey("DownloadBy")]
        public virtual User DownloadByUser { get; set; }
    }
}
