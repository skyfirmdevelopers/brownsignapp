﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.Data {
    public enum SignedStatusEnum {
        Pending = 1,
        Signed = 2,
        Downloaded = 3,
        Zipped = 4,
        Ready = 5,
        StagedForSign = 6
    }
}
