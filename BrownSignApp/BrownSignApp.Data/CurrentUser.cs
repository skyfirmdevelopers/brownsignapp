﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrownSignApp.Api.Models {
    public class CurrentUser {

        public CurrentUser() {
            Roles = new List<string>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int LocationId { get; set; }

        public List<string> Roles { get; set; }
    }
}