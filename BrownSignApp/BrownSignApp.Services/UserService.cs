using BrownSignApp.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;

namespace BrownSignApp.Services {
    public class UserService {

        public List<User> GetAllUsers() {
            List<User> result = new List<User>();
            using (StorageEntities db = new StorageEntities()) {

                result = db.Users
                    .Where(x=> x.IsActive)
                    .Include(x => x.UserRoles)
                    .Include(x => x.UserRoles.Select(t => t.Role))
                    .ToList();
                foreach (User item in result) {
                    db.Entry(item).Reference(x => x.Location).Load();
                }
            }
            return result;
        }

        public List<User> DeleteUser(int userId) {
            using (StorageEntities db = new StorageEntities()) {
                User user = db.Users
                    .Where(x => x.UserId == userId)
                    .FirstOrDefault();
                if (user == null)
                    return GetAllUsers();

                user.IsActive = false;
                db.SaveChanges();
            }
            return GetAllUsers();
        }

        public List<User> Register(User model, List<int> RoleIds) {
            using (StorageEntities db = new StorageEntities()) {
                model.Password = GenerateHashedPassword(model.Password);
                model.IsActive = true;
                db.Users.Add(model);
                db.SaveChanges();
                
                foreach (int roleId in RoleIds) {
                    db.UserRoles.Add(new UserRole { UserId = model.UserId, RoleId = roleId });
                }
                db.SaveChanges();

            }
            return GetAllUsers();
        }

        public User GetUserByUserId(int userId) {
            User result = new User();
            using (StorageEntities db = new StorageEntities()) {
                result = db.Users
                    .Where(x => x.UserId == userId)
                    .FirstOrDefault();
                return result;
            }
        }

        public List<UserRole> GetUserRolesByUserId(int userId) {
            List<UserRole> result = new List<UserRole>();
            using (StorageEntities db = new StorageEntities()) {
                result = db.UserRoles
                    .Where(x => x.UserId == userId)
                    .ToList();
                return result;
            }
        }

        public List<User> Edit(User model, List<int> RoleIds) {
            using (StorageEntities db = new StorageEntities()) {
                User user = db.Users.Find(model.UserId);
                if (user != null) {
                    model.Password = user.Password;
                }
            }

            using (StorageEntities db = new StorageEntities()) {
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                var current = db.UserRoles
                    .Where(x => x.UserId == model.UserId)
                    .ToList();

                var deleted = current.Where(c => !RoleIds.Any(d => c.RoleId == d))
                    .ToList();

                for (int i = 0; i < deleted.Count;i++) {
                    db.Entry(deleted[i]).State = System.Data.Entity.EntityState.Deleted;
                }
                db.SaveChanges();

                var newItems = RoleIds
                    .Where(x => !current.Any(t => t.RoleId == x))
                    .ToList();
                foreach (int roleId in newItems) {
                    db.UserRoles.Add(new UserRole { UserId = model.UserId, RoleId = roleId });
                }
                db.SaveChanges();
            }
            return GetAllUsers();
        }


        public User Login(User model) {
            User result = new User();
            using (StorageEntities db = new StorageEntities()) {
                User user = db.Users
                    .Include("UserRoles")
                    .Include("UserRoles.Role")
                    .Where(x => x.Username == model.Username)
                    .FirstOrDefault();

                if (user == null) {
                    return null;
                }

                bool passwordsMatch = VerifyHashedPassword(user.Password, model.Password);
                if (passwordsMatch == false) {
                    return null;
                }

                result = user;
            }
            return result;
        }

        public void ResetPassword(int UserId, string Password) {
            using (StorageEntities db = new StorageEntities()) {
                User user = db.Users
                    .Where(x => x.UserId == UserId)
                    .FirstOrDefault();

                if (user == null) {
                    return;
                }

                user.Password = GenerateHashedPassword(Password);
                db.SaveChanges();
            }
        }

        public string GenerateHashedPassword(string password) {
            int saltSize = 16;
            int bytesRequired = 32;
            byte[] array = new byte[1 + saltSize + bytesRequired];
            int iterations = 1000; // 1000, afaik, which is the min recommended for Rfc2898DeriveBytes
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, saltSize, iterations)) {
                byte[] salt = pbkdf2.Salt;
                Buffer.BlockCopy(salt, 0, array, 1, saltSize);
                byte[] bytes = pbkdf2.GetBytes(bytesRequired);
                Buffer.BlockCopy(bytes, 0, array, saltSize + 1, bytesRequired);
            }
            return Convert.ToBase64String(array);
        }

        public bool VerifyHashedPassword(string hashedPassword, string password) {
            byte[] buffer4;
            if (hashedPassword == null) {
                return false;
            }
            if (password == null) {
                throw new ArgumentNullException("password");
            }
            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0)) {
                return false;
            }
            byte[] dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            byte[] buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, dst, 0x3e8)) {
                buffer4 = bytes.GetBytes(0x20);
                string test1 = Convert.ToBase64String(buffer3);
                string test = Convert.ToBase64String(buffer4);
            }

            return ByteArraysEqual(buffer3, buffer4);
        }

        public bool ByteArraysEqual(byte[] b1, byte[] b2) {
            if (b1 == b2) return true;
            if (b1 == null || b2 == null) return false;
            if (b1.Length != b2.Length) return false;
            for (int i = 0; i < b1.Length; i++) {
                if (b1[i] != b2[i]) return false;
            }
            return true;
        }
        
    }
}