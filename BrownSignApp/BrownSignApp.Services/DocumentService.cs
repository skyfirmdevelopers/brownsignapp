﻿using BrownSignApp.Api.Models;
using BrownSignApp.Data;
using ImageMagick;
using Ionic.Zip;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Word = Microsoft.Office.Interop.Word;

namespace BrownSignApp.Services {
    public class DocumentService {

        private string PngPath = ConfigurationManager.AppSettings["PngPath"];
        private string UnsignedPath = ConfigurationManager.AppSettings["UnsignedPath"];
        private string SignedPath = ConfigurationManager.AppSettings["SignedPath"];
        private string ZipPath = ConfigurationManager.AppSettings["ZipPath"];
        private FileInfo image { get; set; }

        public DocumentService() {
            
            if (Directory.Exists(UnsignedPath) == false) {
                Directory.CreateDirectory(UnsignedPath);
            }
            if (Directory.Exists(SignedPath) == false) {
                Directory.CreateDirectory(SignedPath);
            }
            if (Directory.Exists(ZipPath) == false) {
                Directory.CreateDirectory(ZipPath);
            }

            if (Directory.Exists(PngPath) == false) {
                Directory.CreateDirectory(PngPath);
            }
        }


        public List<Data.ZipFile> GetSignedZips(int locationId) {
            using (StorageEntities db = new StorageEntities()) {
                var result = db.ZipFiles
                    .Include("Status")
                    .Where(x => x.LocationId == locationId && x.StatusId == (int)SignedStatusEnum.Ready)
                    .ToList();
                return result;
            }
        }

        public decimal DocumentSignProgress() {
            using (StorageEntities db = new StorageEntities()) {
                var query = db.Database.SqlQuery<decimal>("exec GetSignedProgress")
                    .FirstOrDefault();
                return query;
            }
        }

        public List<Document> GetUnsignedDocuments(int locationId = 0) {
            using (StorageEntities db = new StorageEntities()) {
                var query = db.Documents
                        .Include("Status")
                        .Where(x => x.IsActive == true &&
                                    (x.StatusId == (int)SignedStatusEnum.Pending ||
                                    x.StatusId == (int)SignedStatusEnum.StagedForSign ||
                                    x.StatusId == (int)SignedStatusEnum.Signed));
                if (locationId != 0)
                    query = query
                        .Where(x => x.LocationId == locationId);

                return query.ToList();
            }
        }

        public List<Document> GetRequiredSignatureDocuments() {
            using (StorageEntities db = new StorageEntities()) {
                var query = db.Documents
                        .Include("Location")
                        .Include("Status")
                        .Where(x => x.StatusId == (int)SignedStatusEnum.Pending ||
                                    x.StatusId == (int)SignedStatusEnum.StagedForSign);
                return query.ToList();
            }
        }
        

        public List<Document> UpdateDocumentsForSigning(int userId, int[] DocumentIds) {
            using (StorageEntities db = new StorageEntities()) {
                var unsignedDocs = db.Documents
                    .Where(x => x.StatusId == (int)SignedStatusEnum.Pending)
                    .ToList();
                if (unsignedDocs == null || unsignedDocs != null && unsignedDocs.Count == 0)
                    return GetRequiredSignatureDocuments();

                var docsToSign = unsignedDocs
                    .Where(x => DocumentIds.Any(t => x.DocumentId == t))
                    .ToList();
                if (docsToSign == null || docsToSign != null && docsToSign.Count == 0)
                    return GetRequiredSignatureDocuments();

                foreach (Document item in docsToSign) {
                    item.StatusId = (int)SignedStatusEnum.StagedForSign;
                    item.SignedBy = userId;
                    item.SignedDate = DateTime.Now;
                    item.ModifiedBy = userId;
                    item.ModifiedDate = DateTime.Now;
                }
                db.SaveChanges();
            }
            return GetRequiredSignatureDocuments();
        }

        public void SignDocuments() {
            string serverPath = Environment.CurrentDirectory;// HttpContext.Current.Server.MapPath("~");
            string imageFilePath = Path.Combine(serverPath, "signature.png");
            image = new FileInfo(imageFilePath);

            using (StorageEntities db = new StorageEntities()) {
                var docsToSign = db.Documents
                    .Where(x => x.StatusId == (int)SignedStatusEnum.StagedForSign && x.HasPreview)
                    .ToList();
                if (docsToSign == null || docsToSign != null && docsToSign.Count == 0)
                    return;

                foreach (Document item in docsToSign) {
                    try {
                        Console.WriteLine($"Working on the following document: {item.DocumentId} {item.Name}{item.Extension}");
                        if (item.Extension.Contains("doc")) {
                            SignWordDocument(item);
                        } else if (item.Extension.Contains("pdf")) {
                            SignPDFDocument(item);
                        }
                        item.StatusId = (int)SignedStatusEnum.Signed;
                        db.SaveChanges();
                    } catch (Exception ex) {
                        Trace.WriteLine(ex.Message);
                    }
                }
            }
        }
        

        public void SignPDFDocument(Document document) {
            string filePath = Path.Combine(UnsignedPath, document.DocumentId + document.Extension);
            string newPath = Path.Combine(SignedPath, document.DocumentId + document.Extension);

            if (IsFileLocked(new FileInfo(filePath))) {
                throw new Exception("File is in use");
            }
            using (Stream pdfStream = new FileStream(filePath, FileMode.Open))
            using (Stream imageStream = new FileStream(image.FullName, FileMode.Open))
            using (Stream newpdfStream = new FileStream(newPath, FileMode.Create, FileAccess.ReadWrite)) {
                PdfReader pdfReader = new PdfReader(pdfStream);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, newpdfStream);
                PdfContentByte pdfContentByte = pdfStamper.GetOverContent(pdfReader.NumberOfPages);
                iTextSharp.text.Image sigImage = iTextSharp.text.Image.GetInstance(imageStream);
                sigImage.SetAbsolutePosition(105, 38);
                pdfContentByte.AddImage(sigImage);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                pdfContentByte.SetFontAndSize(bf, 10);
                pdfContentByte.BeginText();
                string text = DateTime.Now.ToString();
                pdfContentByte.ShowTextAligned(1, text, 425, 42, 0);
                pdfContentByte.EndText();
                pdfContentByte.BeginText();
                string notifyStamp = "SIGNED BY INTERPRETING MD: THIS DOCUMENT WAS ELECTRONICALLY SIGNED";
                pdfContentByte.ShowTextAligned(1, notifyStamp, 305, 20, 0);
                pdfContentByte.EndText();
                pdfStamper.Close();
            }
        }

        private void SignWordDocument(Document document) {
            string filePath = Path.Combine(UnsignedPath, document.DocumentId + document.Extension);
            string newPath = Path.Combine(SignedPath, document.DocumentId + document.Extension);

            var applicationWord = new Word.Application();
            applicationWord.Visible = false;
            if (IsFileLocked(new FileInfo(filePath))) {
                throw new Exception("File is in use");
            }
            Word.Document doc = applicationWord.Documents.Open(filePath);

            Word.Range footerRange = doc.Sections.First.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
            footerRange.Text = "Signed by Interpreting MD:                                     Date Signed : " + DateTime.Now + Environment.NewLine +
                "This document was electronically signed.";
            Word.InlineShape shape = doc.InlineShapes
                .AddPicture(image.FullName, false, true, footerRange);

            Word.Shape myShape = shape
                .ConvertToShape();
            myShape
                .ZOrder(Microsoft.Office.Core.MsoZOrderCmd.msoSendBehindText);
            myShape.IncrementTop(20);
            myShape.IncrementLeft(140);

            doc.SaveAs2(newPath);
            doc.Close();
            applicationWord.Quit();
        }

        public void PrepareSignedDocuments() {
            using (StorageEntities db = new StorageEntities()) {
                var locations = db.Locations.ToList();
                foreach (Location loc in locations) {
                    var stillSigning = db.Documents
                        .Any(x => x.LocationId == loc.LocationId && x.StatusId == (int)SignedStatusEnum.StagedForSign);
                    if (stillSigning == true)
                        continue;

                    var signedDocs = db.Documents
                        .Where(x => x.LocationId == loc.LocationId && x.StatusId == (int)SignedStatusEnum.Signed)
                        .ToList();

                    if (signedDocs.Count == 0)
                        continue;

                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile()) {
                        Data.ZipFile zipFile = new Data.ZipFile();
                        zipFile.Name = loc.Name + DateTime.Now.ToFileTime() + ".zip";
                        zipFile.LocationId = loc.LocationId;
                        zipFile.CreatedDate = DateTime.Now;
                        zipFile.StatusId = (int)SignedStatusEnum.Ready;
                        db.ZipFiles.Add(zipFile);
                        db.SaveChanges();

                        foreach (Document doc in signedDocs) {
                            try {
                                doc.StatusId = (int)SignedStatusEnum.Zipped;
                                doc.ModifiedDate = DateTime.Now;
                                doc.ZippedDate = DateTime.Now;

                                ZipFileDocument zipFileDocument = new ZipFileDocument();
                                zipFileDocument.DocumentId = doc.DocumentId;
                                zipFileDocument.ZipFileId = zipFile.ZipFileId;
                                zipFileDocument.CreatedDate = DateTime.Now;
                                db.ZipFileDocuments.Add(zipFileDocument);

                                var e = zip.AddFile(Path.Combine(SignedPath, doc.DocumentId + doc.Extension));
                                e.FileName = doc.Name + doc.Extension;
                            } catch (Exception ex) {

                            }
                        }
                        db.SaveChanges();

                        zip.Save(Path.Combine(ZipPath, zipFile.ZipFileId + ".zip"));
                    }
                }
            }
        }

        public List<Document> SaveDocuments(Data.UploadFile[] docs, int UserId, int LocationId) {
            using (StorageEntities db = new StorageEntities()) {
                foreach (Data.UploadFile item in docs) {
                    Document doc = new Document();
                    doc.LocationId = LocationId;
                    doc.Name = Path.GetFileNameWithoutExtension(item.FileName);
                    doc.Extension = Path.GetExtension(item.FileName).ToLower();
                    doc.StatusId = (int)SignedStatusEnum.Pending;
                    doc.IsActive = true;
                    doc.UploadDate = DateTime.Now;
                    doc.UploadBy = UserId;
                    db.Documents.Add(doc);
                    db.SaveChanges();

                    string filePath = Path.Combine(UnsignedPath, doc.DocumentId.ToString() + Path.GetExtension(item.FileName).ToLower());
                    var stream = new FileStream(filePath, FileMode.Create);
                    item.InputStream.CopyTo(stream);
                    stream.Close();
                }
            }
            return GetUnsignedDocuments(LocationId);
        }

        public List<string> GetImagesForPreview(int documentId) {
            List<string> result = new List<string>();

            string[] documentPngs = Directory.GetFiles(PngPath, documentId + "-*.png");
            foreach (string item in documentPngs) {
                result.Add(Convert.ToBase64String(File.ReadAllBytes(item)));
            }
            return result;
        }

        

        public void CreatePreviewImages() {
            using (StorageEntities db = new StorageEntities()) {
                var documents = db.Documents
                    .Where(x => x.HasPreview == false && x.IsActive)
                    .ToList();
                foreach (Document item in documents) {
                    try {
                        Console.WriteLine($"Working on the following document: {item.DocumentId} {item.Name}{item.Extension}");
                        string filePath = Path.Combine(UnsignedPath, item.DocumentId + item.Extension);
                        int documentId = item.DocumentId;
                        string extension = item.Extension;
                        if (extension == ".docx") {
                            CreatePreviewImagesDocx(filePath, documentId);
                        } else if (extension == ".pdf") {
                            CreatePreviewImagesPdf(filePath, documentId);
                        }
                        item.HasPreview = true;
                        db.SaveChanges();
                    } catch (Exception ex) {
                        Trace.WriteLine(ex.Message);
                    }                 
                }
            }
        }

        public void CreatePreviewImagesPdf(string filePath, int documentId) {
            if (IsFileLocked(new FileInfo(filePath))) {
                throw new Exception("File is in use");
            }

            MagickReadSettings settings = new MagickReadSettings();
            // Settings the density to 300 dpi will create an image with a better quality
            settings.Density = new Density(300, 300);
            
            using (MagickImageCollection images = new MagickImageCollection()) {
                // Add all the pages of the pdf file to the collection
                images.Read(filePath.ToString(), settings);

                int page = 1;
                foreach (MagickImage image in images) {
                    image.Write(Path.Combine(PngPath, documentId + "-" + page + ".png"));
                    page++;
                }
            }
        }

        public void CreatePreviewImagesDocx(string filePath, int documentId) {
            if (IsFileLocked(new FileInfo(filePath))) {
                throw new Exception("File is in use");
            }

            Word.Application word = new Word.Application();
            object oMissing = System.Reflection.Missing.Value;
            word.Visible = false;
            word.ScreenUpdating = false;
            Object filename = (Object)filePath;
            
            Word.Document doc = word.Documents.Open(ref filename, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            doc.Activate();

            object outputFileName = filePath.Replace(".docx", ".pdf");
            object fileFormat = Word.WdSaveFormat.wdFormatPDF;
            doc.SaveAs(ref outputFileName,
                ref fileFormat, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            object saveChanges = Word.WdSaveOptions.wdDoNotSaveChanges;
            ((Word._Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
            doc = null;
            ((Word._Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;

            CreatePreviewImagesPdf(outputFileName.ToString(), documentId);
        }

        public byte[] GetZipForDownload(int zipFileId, int userId, out string fileName) {
            using (StorageEntities db = new StorageEntities()) {
                var zipFile = db.ZipFiles
                    .Where(x => x.ZipFileId == zipFileId)
                    .FirstOrDefault();
                fileName = string.Empty;
                if (zipFile == null)
                    return new byte[0];

                zipFile.DownloadBy = userId;
                zipFile.DownloadDate = DateTime.Now;
                zipFile.StatusId = (int)SignedStatusEnum.Downloaded;
                db.SaveChanges();
                fileName = zipFile.Name;
                return File.ReadAllBytes(Path.Combine(ZipPath, zipFile.ZipFileId + ".zip"));
            }
        }

        public List<Document> DeleteDocument(int documentId, int LocationId) {
            using (StorageEntities db = new StorageEntities()) {
                Document document = db.Documents
                    .Where(x => x.DocumentId == documentId)
                    .FirstOrDefault();
                if (document == null)
                    return GetUnsignedDocuments(LocationId);

                document.IsActive = false;
                db.SaveChanges();
            }
            return GetUnsignedDocuments(LocationId);
        }

        protected virtual bool IsFileLocked(FileInfo file) {
            FileStream stream = null;

            try {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            } catch (IOException) {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            } finally {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

    }
}