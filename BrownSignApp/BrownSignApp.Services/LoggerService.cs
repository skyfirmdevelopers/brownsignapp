﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownSignApp.Services {
    public class LoggerService {

        public void Log(Exception ex) {

        }

        public void Log(string message) {
            string log = DateTime.Now.ToString() + " - " + message;
            Trace.WriteLine(log);
        }

    }
}
