﻿using BrownSignApp.Data;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BrownSignApp.Services {
    public class LocationService {

        public List<Location> GetAllLocations() {
            List<Location> result = new List<Location>();
            using (StorageEntities db = new StorageEntities()) {
                result = db.Locations
                    .ToList();
            }
            return result;
        }

        public List<Location> Create(Location model) {
            using (StorageEntities db = new StorageEntities()) {
                db.Locations.Add(model);
                db.SaveChanges();
            }
            return GetAllLocations();
        }

        public List<Location> Delete(int LocationId) {
            using (StorageEntities db = new StorageEntities()) {
                Location location = db.Locations
                    .Where(x => x.LocationId == LocationId)
                    .FirstOrDefault();
                if (location == null)
                    return GetAllLocations();

                location.IsActive = false;
                db.SaveChanges();
            }
            return GetAllLocations();
        }

        public Location GetLocationByLocationId(int LocationId) {
            Location result = new Location();
            using (StorageEntities db = new StorageEntities()) {
                result = db.Locations
                    .Where(x => x.LocationId == LocationId)
                    .FirstOrDefault();
                return result;
            }
        }

        public List<Location> Edit(Location model) {
            using (StorageEntities db = new StorageEntities()) {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
            }
            return GetAllLocations();
        }
    }
}